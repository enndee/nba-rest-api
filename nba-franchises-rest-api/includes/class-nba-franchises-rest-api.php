<?php
/**
* Main class.
*
* @package Nba_Franchises_Rest_Api
*/
class Nba_Franchises_Rest_Api {
    /**
     * Returns the instance.
     */
    public static function get_instance() {
        static $instance = null;
        if ( is_null( $instance ) ) {
            $instance = new self();
        }
        return $instance;
    }
    /**
     * Constructor method.
     */
    private function __construct() {
        $this->includes();
    }
    // Includes
    public function includes() {
    }
}