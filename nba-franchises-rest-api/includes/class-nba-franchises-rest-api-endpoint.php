<?php
/**
 * franchise endpoints.
 */
class NBA_Franchises_REST_API_Endpoint extends WP_REST_Controller {
    /**
	 * Constructor.
	 */
	public function __construct() {
		$this->namespace = 'nba/v1';
		$this->rest_base = 'franchises';
        $this->post_type = 'franchises';
	}
    
    /**
     * Register the component routes.
     */
    public function register_routes() {
        register_rest_route( $this->namespace, '/' . $this->rest_base, array(
            array(
                'methods'             => WP_REST_Server::READABLE,
                'callback'            => array( $this, 'get_items' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' ),
                'args'                => $this->get_collection_params(),
            )
        ) );
        // Search for a specific franchise - /nba/v1/franchises/golden+state+warriors
        register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<name>[\S]+)', array(            
            array(
                'methods'             => WP_REST_Server::READABLE,
                'callback'            => array( $this, 'get_item' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' ),
            )
        ) );
    }

    public function get_item( $request ) {
        // Replace the '+' in the name with a space and search for that franchise's page.
        $franchise_name = get_page_by_title(str_replace('+', ' ', $request['name']), OBJECT, 'franchises');
        // Get the post by using the ID of the post.
        $franchise = get_post($franchise_name->ID);

        if ( empty( $franchise ) ) {
            return new WP_Error( 'no_team', 'Team not found', array( 'status' => 404 ) );
        }
 
        $response = $this->prepare_item_for_response( $franchise, $request );
 
        // Return all of our post response data.
        return $response;        
    }

    /**
     * Retrieve franchises.
     */
    public function get_items( $request ) {
        $meta_query = array();

        if(isset($request['conference'])) {
            array_push($meta_query, array('key' => 'conference', 'value' => $request['conference']));
        }

        $args = array(
            'post_type'      => $this->post_type,
            'posts_per_page' => $request['per_page'],
            'page'           => $request['page'],
            'meta_query'     => $meta_query,
        );

        $franchises = get_posts( $args );
        $data = array();

        if ( $franchises ) {
            foreach ( $franchises as $franchise ) :
                $itemdata = $this->prepare_item_for_response( $franchise, $request );
                $data[] = $this->prepare_response_for_collection( $itemdata );
            endforeach;
        }

        $data = rest_ensure_response( $data );
        return $data;
    }

    /**
     * Check if a given request has access to franchise items.
     */
    public function get_items_permissions_check( $request ) {
        return true;
    }

    /**
     * Get the query params for collections
     */
    public function get_collection_params() {
        return array(
            'page'     => array(
                'description'       => 'Current page of the collection.',
                'type'              => 'integer',
                'default'           => 1,
                'sanitize_callback' => 'absint',
            ),
            'per_page' => array(
                'description'       => 'Maximum number of items to be returned in result set.',
                'type'              => 'integer',
                'default'           => 30,
                'sanitize_callback' => 'absint',              
            ),
            'conference'  => array(
                'description'       => 'Player conference.',
                'type'              => 'string',
            ),            
        );
    }

    /**
     * Prepares franchise data for return as an object.
     */
    public function prepare_item_for_response( $franchise, $request ) {
        $data = array(
            'id'      => $franchise->ID,
            'name'    => $franchise->post_title,
            'conference' => $franchise->conference,
            'founded' => $franchise->founded,
            'arena' => $franchise->arena,
            'owner' => $franchise->owner,
            'g_league' => $franchise->g_league,
            'franchise_website' => $franchise->franchise_website
        );

        return $data;
    }
}