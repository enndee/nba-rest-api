<?php 
/**
 * Plugin Name: NBA Franchise Rest API
 * Description: Custom REST API for the NBA Franchise data.
 * Version: 1.0
 * Author: ND
 * Author URI:
 * License: GPL2+
 *
 * @package Rest_Api_Tutorial
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define constants.
define( 'NBA_FRANCHISES_REST_API_PLUGIN_VERSION', '2.0.0' );
define( 'NBA_FRANCHISES_REST_API_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );

// Include the main class.
require plugin_dir_path( __FILE__ ) . 'includes/class-nba-franchises-rest-api.php';

// Main instance of plugin.
function nba_franchises_rest() {
    return Nba_Franchises_Rest_Api::get_instance();
}

// Global for backwards compatibility.
$GLOBALS['nba_franchises_rest'] = nba_franchises_rest();

add_action( 'rest_api_init', function () {	
		require plugin_dir_path( __FILE__ ) . 'includes/class-nba-franchises-rest-api-endpoint.php';
		$controller = new NBA_Franchises_REST_API_Endpoint();
		$controller->register_routes();	
} );