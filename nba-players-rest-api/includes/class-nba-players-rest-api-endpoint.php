<?php
/**
 * player endpoints.
 */
class NBA_Players_REST_API_Endpoint extends WP_REST_Controller {
    /**
	 * Constructor.
	 */
	public function __construct() {
		$this->namespace = 'nba/v1';
		$this->rest_base = 'players';
        $this->post_type = 'players';
	}
    
    /**
     * Register the component routes.
     */
    public function register_routes() {
        register_rest_route( $this->namespace, '/' . $this->rest_base, array(
            array(
                'methods'             => WP_REST_Server::READABLE,
                'callback'            => array( $this, 'get_items' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' ),
                'args'                => $this->get_collection_params(),
            )
        ) );
        // Search for a specific player - /nba/v1/players/stephen+curry
        // register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<name>[\S]+)', array(            
        //     array(
        //         'methods'             => WP_REST_Server::READABLE,
        //         'callback'            => array( $this, 'get_item' ),
        //         'permission_callback' => array( $this, 'get_items_permissions_check' ),
        //     )
        // ) );

        register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<id>\d+)', array(            
            array(
                'methods'             => WP_REST_Server::READABLE,
                'callback'            => array( $this, 'get_item' ),
                'permission_callback' => array( $this, 'get_items_permissions_check' )
            )
        ) );
    }

    public function get_item( $request ) {

        // Get the post by using the ID of the post.
        $player = get_post($request['id']);

        if ( empty( $player ) ) {
            return rest_ensure_response( array() );
        }
 
        $response = $this->prepare_item_for_response( $player, $request );
 
        // Return all of our post response data.
        return $response;        
    }

    // Works with the 'search for a specific player' route above. Replaced it with a search by player ID as 
    // IDs are unique, but names aren't.
    
    // public function get_item( $request ) {
    //     // Remove the '+' from the query and replace with a space.
    //     $sanitized = str_replace('+', ' ', $request['name']);
    //     // sanitize_title helps remove apostrophes and replaces the space with a hyphen.
    //     $sanitized = sanitize_title($sanitized);    
    //     $player_name = get_page_by_path($sanitized, OBJECT, 'players');
    //     // Get the post by using the ID of the post.
    //     $player = get_post($player_name->ID);

    //     if ( empty( $player ) ) {
    //         return rest_ensure_response( array() );
    //     }
 
    //     $response = $this->prepare_item_for_response( $player, $request );
 
    //     // Return all of our post response data.
    //     return $response;        
    // }

    /**
     * Retrieve players.
     */
    public function get_items( $request ) {
        $meta_query = array();
            
        if(isset($request['team'])) {
            array_push($meta_query, array('key' => 'team', 'value' => $request['team']));
        }

        if(isset($request['position'])) {
            array_push($meta_query, array('key' => 'position', 'value' => $request['position']));
        }
        
        if(isset($request['number'])) {
            array_push($meta_query, array('key' => 'number', 'value' => $request['number']));
        }

        $args = array(
            'post_type'      => $this->post_type,
            'posts_per_page' => $request['per_page'],
            'page'           => $request['page'],
            'meta_query'     => $meta_query,
        );

        $players = get_posts( $args );
        $data = array();

        if ( $players ) {
            foreach ( $players as $player ) :
                $itemdata = $this->prepare_item_for_response( $player, $request );
                $data[] = $this->prepare_response_for_collection( $itemdata );
            endforeach;
        }

        $data = rest_ensure_response( $data );
        return $data;
    }

    /**
     * Check if a given request has access to player items.
     */
    public function get_items_permissions_check( $request ) {
        return true;
    }

    /**
     * Get the query params for collections
     */
    public function get_collection_params() {
        return array(
            'page'     => array(
                'description'       => 'Current page of the collection.',
                'type'              => 'integer',
                'default'           => 1,
                'sanitize_callback' => 'absint',
            ),
            'per_page' => array(
                'description'       => 'Maximum number of items to be returned in result set.',
                'type'              => 'integer',
                'default'           => 500,
                'sanitize_callback' => 'absint',              
            ),             
            'position'  => array(
                'description'       => 'Player position.',
                'type'              => 'string',
            ),
            'team'  => array(
                'description'       => 'Team name.',
                'type'              => 'string',
            ),
            'number'  => array(
                'description'       => 'Player number.',
                'type'              => 'int',
            ),
        );
    }

    /**
     * Prepares player data for return as an object.
     */
    public function prepare_item_for_response( $player, $request ) {
        $data = array(
            'id'      => $player->ID,
            'name'    => $player->post_title,
            'team' => $player->team,
            'number' => $player->number,
            'position' => $player->position,
            'height' => $player->height,
            'DOB' => $player->dob
        );

        return $data;
    }
}