<?php 
/**
 * Plugin Name: NBA Player Rest API
 * Description: Custom REST API for the NBA Player data.
 * Version: 1.0
 * Author: ND
 * Author URI:
 * License: GPL2+
 *
 * @package Rest_Api_Tutorial
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define constants.
define( 'NBA_PLAYERS_REST_API_PLUGIN_VERSION', '2.0.0' );
define( 'NBA_PLAYERS_REST_API_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );

// Include the main class.
require plugin_dir_path( __FILE__ ) . 'includes/class-nba-players-rest-api.php';

// Main instance of plugin.
function nba_players_rest() {
    return Nba_Players_Rest_Api::get_instance();
}

// Global for backwards compatibility.
$GLOBALS['nba_players_rest'] = nba_players_rest();

add_action( 'rest_api_init', function () {
	// if ( class_exists( 'Players' ) ) { 
		require plugin_dir_path( __FILE__ ) . 'includes/class-nba-players-rest-api-endpoint.php';
		$controller = new NBA_Players_REST_API_Endpoint();
		$controller->register_routes();
	// }
} );